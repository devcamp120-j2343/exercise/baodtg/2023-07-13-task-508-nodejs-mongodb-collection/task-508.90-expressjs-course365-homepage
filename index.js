//Khai báo thư viện express
const express = require('express');

//khai báo thu vien path
const path = require('path')

//khởi tạo app express
const app = express();

//khởi tạo cổng port:
const port = 8000;

app.use(express.static(__dirname + "/views"))

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/index.html"))
})



//chạy app:
app.listen(port, () => {
    console.log(`App listen on port ${port}`)
})